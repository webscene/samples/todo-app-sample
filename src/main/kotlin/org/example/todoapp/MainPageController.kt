package org.example.todoapp

import org.example.todoapp.view.pageFooter
import org.example.todoapp.view.mainPageSection
import org.w3c.dom.*
import org.webscene.core.dom.DomEditor
import org.webscene.core.dom.DomQuery
import org.webscene.core.html.element.input.InputType
import org.webscene.core.html.element.toDomElement
import org.webscene.core.html.HtmlCreator as html
import org.webscene.core.html.HtmlInputCreator as htmlInput

/**
 * Controller for the main page. Based on kotlin_todo's [View.kt file](https://github.com/programiz/kotlin-todo/blob/master/src/View.kt).
 */
internal class MainPageController {
    private val newTodo: HTMLInputElement
    private val todoList: HTMLUListElement

    init {
        val footer = pageFooter()
        val section = mainPageSection()
        DomEditor.editSection(domElement = footer.toDomElement())
        DomEditor.editSection(domElement = section.toDomElement())
        newTodo = DomQuery.elementById("new-todo")
        todoList = DomQuery.elementById("todo-list")
    }

    /** Display given Todo_items in the UI. */
    fun displayItems(items: List<TodoListItem>) {
        todoList.innerHTML = ""
        items.forEach { addItem(it) }
    }

    /** Sets the correct filter classes. */
    fun setFilter(route: String) {
        val selectedFilter = DomQuery.elementBySelector<HTMLElement>(".filters .selected")
        val currentPage = DomQuery.elementBySelector<HTMLElement>(".filters [href=\"#/$route\"]")
        selectedFilter.className = ""
        currentPage.className = "selected"
    }

    /** Clears new Todo_input. */
    fun clearNewTodoInput() {
        newTodo.value = ""
    }

    /** Adds item to the UI. */
    fun addItem(item: TodoListItem) {
        // Create List item.
        val todoElement = html.parentElement("li") {
            attributes["data-id"] = item.id
            if (item.completed) classes += "completed"
            children += htmlInput.input(InputType.CHECKBOX) {
                classes += "toggle"
                if (item.completed) attributes["checked"] = "true"
            }
            parentHtmlElement("label") { +item.title }
            children += htmlInput.input(InputType.BUTTON) { classes += "destroy" }
        }
        todoList.append(todoElement.toDomElement())
    }

    /** Removes item from the UI. */
    fun removeItem(id: String) {
        val todoElement = DomQuery.elementBySelector<HTMLLIElement>("[data-id=\"$id\"]")
        todoList.removeChild(todoElement)
    }

    /** Toggle item's completed status in the UI. */
    fun toggleItem(id: String, completed: Boolean) {
        val todoElement = DomQuery.elementBySelector<HTMLLIElement>("[data-id=\"$id\"]")
        todoElement.className = if (completed) "completed" else ""
    }
}
