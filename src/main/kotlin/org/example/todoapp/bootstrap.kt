package org.example.todoapp

// Bootstrap's the app.

@Suppress("unused")
fun main(args: Array<String>) {
    val dbName = "todoList"
    App(dbName)
}
