package org.example.todoapp.view

import org.webscene.core.html.element.UnorderedListElement
import org.webscene.core.html.element.input.InputType
import org.webscene.core.html.HtmlInputCreator as htmlInput
import org.webscene.core.html.HtmlCreator as html

// Provides the view for the main page.

private val sectionFooterData = arrayOf("#/" to "All", "#/active" to "Active", "#/completed" to "Completed")

private fun createBottomSection() = html.footer {
    children += html.unorderedList {
        classes += "filters"
        populateFiltersList(this)
    }
}

private fun populateFiltersList(list: UnorderedListElement) {
    sectionFooterData.forEachIndexed { pos, (url, txt) ->
        list.listItem { children += createLink(url = url, txt = txt, selected = pos == 0) }
    }
}

private fun createLink(url: String, txt: String, selected: Boolean) = htmlInput.link(url) {
    if (selected) classes += "selected"
    +txt
}

private fun createTopSection() = html.header {
    classes += "header"
    children += html.heading { +"Todos" }
    children += htmlInput.input(InputType.TEXT, autoFocus = true) {
        id = "new-todo"
        classes += "new-todo"
        attributes["placeholder"] = "What needs to be done?"
    }
}

private fun createMiddleSection() = html.parentElement("section") {
    classes += "main"
    children += createToggleAllCheckbox()
    children += createToggleAllLabel()
    children += createTodoList()
}

private fun createTodoList() = html.unorderedList {
    id = "todo-list"
    classes += "todo-list"
}

private fun createToggleAllLabel() = html.parentElement("label") {
    attributes["for"] = "toggle-all"
    +"Mark all as complete"
}

private fun createToggleAllCheckbox() = htmlInput.input(InputType.CHECKBOX) {
    id = "toggle-all"
    classes += "toggle-all"
}

internal fun mainPageSection() = html.parentElement("section") {
    classes += "todoapp"
    children += createTopSection()
    children += createMiddleSection()
    children += createBottomSection()
}
