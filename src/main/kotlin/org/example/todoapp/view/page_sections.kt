package org.example.todoapp.view

import org.webscene.core.html.HtmlCreator as html
import org.webscene.core.html.HtmlInputCreator as htmlInput

internal fun pageFooter() = html.footer {
    classes += "info"
    children += html.paragraph {
        children += html.span { +"Source on " }
        children += htmlInput.link("https://gitlab.com/webscene/todo-app-sample") { +"Gitlab" }
    }
}
